package com.epam.algo;

import org.junit.Before;
import org.junit.Test;

import static com.epam.algo.Algorithms.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class AlgorithmsTest {
    private int[] intArray;
    private int[] ascendingSortedArray;
    private int[] descendingSortedArray;

    @Before
    public void beforeTest() {
        intArray = new int[]{-573, 261, -474, 780, -561, 0, 728, 996, 600, 242, 760, -243, -603, -907, -493};
        ascendingSortedArray
                = new int[]{-907, -603, -573, -561, -493, -474, -243, 0, 242, 261, 600, 728, 760, 780, 996};
        descendingSortedArray
                = new int[]{996, 780, 760, 728, 600, 261, 242, 0, -243, -474, -493, -561, -573, -603, -907};
    }

    @Test
    public void testBubbleSortAscending() {
        bubbleSort(null);
        bubbleSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testBubbleSortDescending() {
        bubbleSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testSelectionSortAscending() {
        selectionSort(null);
        selectionSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testSelectionSortDescending() {
        selectionSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testInsertionSortAscending() {
        insertionSort(null);
        insertionSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testInsertionSortDescending() {
        insertionSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testQuickSortAscending() {
        quickSort(null);
        quickSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testQuickSortDescending() {
        quickSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testMergeSortAscending() {
        mergeSort(null);
        mergeSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testMergeSortDescending() {
        mergeSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testRadixSortAscending() {
        radixSortLSD(null);
        radixSortLSD(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testRadixSortDescending() {
        radixSortLSD(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testBinarySearchIndexFound() {
        assertThat(binarySearch(ascendingSortedArray, 0), equalTo(7));
    }

    @Test
    public void testBinarySearchIndexNotFound() {
        assertThat(binarySearch(ascendingSortedArray, 13), equalTo(-1));
        assertThat(binarySearch(null, 13), equalTo(-1));
    }

    @Test
    public void testNonRecursiveMergeSortAscending() {
        nonRecursiveMergeSort(null);
        nonRecursiveMergeSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testNonRecursiveMergeSortDescending() {
        nonRecursiveMergeSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }

    @Test
    public void testNonRecursiveQuickSortAscending() {
        nonRecursiveQuickSort(null);
        nonRecursiveQuickSort(intArray);
        assertThat(intArray, equalTo(ascendingSortedArray));
    }

    @Test
    public void testNonRecursiveQuickSortDescending() {
        nonRecursiveQuickSort(intArray, false);
        assertThat(intArray, equalTo(descendingSortedArray));
    }
}
