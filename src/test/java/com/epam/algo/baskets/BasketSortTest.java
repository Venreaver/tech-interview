package com.epam.algo.baskets;

import org.junit.Test;

import static com.epam.algo.baskets.BasketSort.sortBaskets;
import static com.epam.algo.baskets.BasketSort.sortBasketsLikeDutchFlag;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class BasketSortTest {
    @Test
    public void testSortBasketsLikeDutchFlag() {
        Basket[] dutchFlagBaskets = new Basket[10];
        dutchFlagBaskets[0] = new Basket(Color.WHITE);
        dutchFlagBaskets[1] = new Basket(Color.RED);
        dutchFlagBaskets[2] = new Basket(Color.GREEN);
        dutchFlagBaskets[3] = new Basket(Color.GREEN);
        dutchFlagBaskets[4] = new Basket(Color.WHITE);
        dutchFlagBaskets[5] = new Basket(Color.RED);
        dutchFlagBaskets[6] = new Basket(Color.WHITE);
        dutchFlagBaskets[7] = new Basket(Color.RED);
        dutchFlagBaskets[8] = new Basket(Color.WHITE);
        dutchFlagBaskets[9] = new Basket(Color.GREEN);

        Basket[] sortedDutchFlagBaskets = new Basket[10];
        sortedDutchFlagBaskets[0] = new Basket(Color.RED);
        sortedDutchFlagBaskets[1] = new Basket(Color.RED);
        sortedDutchFlagBaskets[2] = new Basket(Color.RED);
        sortedDutchFlagBaskets[3] = new Basket(Color.WHITE);
        sortedDutchFlagBaskets[4] = new Basket(Color.WHITE);
        sortedDutchFlagBaskets[5] = new Basket(Color.WHITE);
        sortedDutchFlagBaskets[6] = new Basket(Color.WHITE);
        sortedDutchFlagBaskets[7] = new Basket(Color.GREEN);
        sortedDutchFlagBaskets[8] = new Basket(Color.GREEN);
        sortedDutchFlagBaskets[9] = new Basket(Color.GREEN);

        sortBasketsLikeDutchFlag(null);
        sortBasketsLikeDutchFlag(dutchFlagBaskets);
        assertThat(dutchFlagBaskets, equalTo(sortedDutchFlagBaskets));
    }

    @Test
    public void testSortBaskets() {
        Basket[] baskets = new Basket[14];
        baskets[0] = new Basket(Color.WHITE);
        baskets[1] = new Basket(Color.WHITE);
        baskets[2] = new Basket(Color.WHITE);
        baskets[3] = new Basket(Color.OTHERCOLOR);
        baskets[4] = new Basket(Color.GREEN);
        baskets[5] = new Basket(Color.GREEN);
        baskets[6] = new Basket(Color.OTHERCOLOR);
        baskets[7] = new Basket(Color.OTHERCOLOR);
        baskets[8] = new Basket(Color.GREEN);
        baskets[9] = new Basket(Color.GREEN);
        baskets[10] = new Basket(Color.RED);
        baskets[11] = new Basket(Color.RED);
        baskets[12] = new Basket(Color.OTHERCOLOR);
        baskets[13] = new Basket(Color.RED);

        Basket[] sortedBaskets = new Basket[14];
        sortedBaskets[0] = new Basket(Color.RED);
        sortedBaskets[1] = new Basket(Color.RED);
        sortedBaskets[2] = new Basket(Color.RED);
        sortedBaskets[3] = new Basket(Color.WHITE);
        sortedBaskets[4] = new Basket(Color.WHITE);
        sortedBaskets[5] = new Basket(Color.WHITE);
        sortedBaskets[6] = new Basket(Color.GREEN);
        sortedBaskets[7] = new Basket(Color.GREEN);
        sortedBaskets[8] = new Basket(Color.GREEN);
        sortedBaskets[9] = new Basket(Color.GREEN);
        sortedBaskets[10] = new Basket(Color.OTHERCOLOR);
        sortedBaskets[11] = new Basket(Color.OTHERCOLOR);
        sortedBaskets[12] = new Basket(Color.OTHERCOLOR);
        sortedBaskets[13] = new Basket(Color.OTHERCOLOR);

        sortBaskets(baskets);
        assertThat(baskets, equalTo(sortedBaskets));
    }

}
