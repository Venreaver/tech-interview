package com.epam.algo.datastructures;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class LinkedListTest {
    private LinkedList<String> testList;

    @Before
    public void setUp() {
        testList = new LinkedList<>();
        testList.add("SuperLinkedList");
        testList.add("is");
        testList.add("This");
        testList.add("! ");
        testList.add("Hello");
    }

    @Test
    public void testGetSize() {
        assertThat(testList.getSize(), is(5));
    }

    @Test
    public void testGetFirst() {
        assertThat(testList.getFirst(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetFirstException() {
        testList.clear();
        testList.getFirst();
    }

    @Test
    public void testIsEmpty() {
        testList.clear();
        assertThat(testList.isEmpty(), is(true));
    }

    @Test
    public void testAddFirst() {
        testList.addFirst("Hmm");
        assertThat(testList.getSize(), is(6));
        assertThat(testList.getFirst(), equalTo("Hmm"));
    }

    @Test
    public void testAdd() {
        assertThat(testList.add("Hmm"), is(true));
    }

    @Test
    public void testRemoveFirst() {
        assertThat(testList.removeFirst(), equalTo("Hello"));
        assertThat(testList.getSize(), is(4));
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveFirstException() {
        testList.clear();
        testList.removeFirst();
    }

    @Test
    public void testContains() {
        testList.add(null);
        assertThat(testList.contains(null), is(true));
        assertThat(testList.contains("Hello"), is(true));
        assertThat(testList.contains("Wow"), is(false));
    }

    @Test
    public void testGet() {
        assertThat(testList.get(3), is("is"));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetExceptionLow() {
        testList.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetExceptionHigh() {
        testList.get(8);
    }

    @Test
    public void testClear() {
        testList.clear();
        assertThat(testList.getSize(), is(0));
    }
}
