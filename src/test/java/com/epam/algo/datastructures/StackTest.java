package com.epam.algo.datastructures;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class StackTest {
    private Stack<String> testStack;

    @Before
    public void setUp() {
        testStack = new Stack<>();
        testStack.push("SuperLinkedList");
        testStack.push("is");
        testStack.push("This");
        testStack.push("! ");
        testStack.push("Hello");
    }

    @Test
    public void testGetSize() {
        assertThat(testStack.getSize(), is(5));
    }

    @Test
    public void testPeek() {
        assertThat(testStack.peek(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testPeekException() {
        testStack.clear();
        testStack.peek();
    }

    @Test
    public void testIsEmpty() {
        testStack.clear();
        assertThat(testStack.isEmpty(), is(true));
    }

    @Test
    public void testPush() {
        testStack.push("Hmm");
        assertThat(testStack.getSize(), is(6));
        assertThat(testStack.peek(), equalTo("Hmm"));
    }

    @Test
    public void testPop() {
        assertThat(testStack.pop(), equalTo("Hello"));
        assertThat(testStack.getSize(), is(4));
    }

    @Test(expected = NoSuchElementException.class)
    public void testPopException() {
        testStack.clear();
        testStack.pop();
    }

    @Test
    public void testClear() {
        testStack.clear();
        assertThat(testStack.getSize(), is(0));
    }
}
