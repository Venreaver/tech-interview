package com.epam.algo.datastructures;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class DequeOnStackTest {
    private DequeOnStack<String> testDeque;

    @Before
    public void setUp() {
        testDeque = new DequeOnStack<>();
        testDeque.add("Hello");
        testDeque.add("! ");
        testDeque.add("This");
        testDeque.add("is");
        testDeque.add("SuperLinkedList");
    }

    @Test
    public void testGetSize() {
        assertThat(testDeque.getSize(), is(5));
    }

    @Test
    public void testAdd() {
        testDeque.add("Hmm");
        assertThat(testDeque.getSize(), is(6));
        assertThat(testDeque.getLast(), equalTo("Hmm"));
    }

    @Test
    public void testAddFirst() {
        testDeque.addFirst("Hmm");
        assertThat(testDeque.getSize(), is(6));
        assertThat(testDeque.element(), equalTo("Hmm"));
    }

    @Test
    public void testAddLast() {
        testDeque.addLast("Hmm");
        assertThat(testDeque.getSize(), is(6));
        assertThat(testDeque.getLast(), equalTo("Hmm"));
    }

    @Test
    public void testElement() {
        assertThat(testDeque.element(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testElementException() {
        testDeque.clear();
        testDeque.element();
    }

    @Test
    public void testGetFirst() {
        assertThat(testDeque.getFirst(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetFirstException() {
        testDeque.clear();
        testDeque.getFirst();
    }

    @Test
    public void testGetLast() {
        assertThat(testDeque.getLast(), equalTo("SuperLinkedList"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetLastException() {
        testDeque.clear();
        testDeque.getLast();
    }

    @Test
    public void testRemove() {
        assertThat(testDeque.remove(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveException() {
        testDeque.clear();
        testDeque.remove();
    }

    @Test
    public void testRemoveFirst() {
        assertThat(testDeque.removeFirst(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveFirstException() {
        testDeque.clear();
        testDeque.removeFirst();
    }

    @Test
    public void testRemoveLast() {
        assertThat(testDeque.removeLast(), equalTo("SuperLinkedList"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveLastException() {
        testDeque.clear();
        testDeque.removeLast();
    }

    @Test
    public void testClear() {
        testDeque.clear();
        assertThat(testDeque.getSize(), is(0));
    }

    @Test
    public void testIsEmpty() {
        testDeque.clear();
        assertThat(testDeque.isEmpty(), is(true));
    }
}
