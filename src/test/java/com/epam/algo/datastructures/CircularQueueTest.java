package com.epam.algo.datastructures;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class CircularQueueTest {
    private CircularQueue<String> testQueue;

    @Before
    public void setUp() {
        testQueue = new CircularQueue<>(6);
        testQueue.add("Hello");
        testQueue.add("! ");
        testQueue.add("This");
        testQueue.add("is");
        testQueue.add("SuperLinkedList");
    }

    @Test
    public void testGetSize() {
        assertThat(testQueue.getSize(), is(5));
    }

    @Test
    public void testAdd() {
        testQueue.add("NewOne");
        assertThat(testQueue.getSize(), is(6));
    }

    @Test(expected = IllegalStateException.class)
    public void testAddException() {
        testQueue.add("NewOne");
        testQueue.add("OneMore");
    }

    @Test
    public void testRemove() {
        assertThat(testQueue.remove(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveException() {
        testQueue.clear();
        testQueue.remove();
    }

    @Test
    public void testElement() {
        assertThat(testQueue.element(), equalTo("Hello"));
    }

    @Test(expected = NoSuchElementException.class)
    public void testElementException() {
        testQueue.clear();
        testQueue.element();
    }

    @Test
    public void testClear() {
        testQueue.clear();
        assertThat(testQueue.getSize(), is(0));
    }

    @Test
    public void testIsEmpty() {
        testQueue.clear();
        assertThat(testQueue.isEmpty(), is(true));
    }

    @Test
    public void testCircle() {
        testQueue.remove();
        testQueue.remove();
        testQueue.add("NewOne");
        testQueue.add("NewTwo");
        testQueue.remove();
        testQueue.add("NewThree");
        testQueue.remove();
        testQueue.add("NewFour");
        testQueue.remove();
        testQueue.add("NewFive");
        testQueue.remove();
        testQueue.add("NewSix");
        assertThat(testQueue.getSize(), is(5));
    }
}
