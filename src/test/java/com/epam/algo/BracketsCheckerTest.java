package com.epam.algo;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class BracketsCheckerTest {
    @Test
    public void testIsBracketsBalanced() {
        BracketsChecker checker = new BracketsChecker();
        assertThat(checker.isBracketsBalanced("]"), is(false));
        assertThat(checker.isBracketsBalanced("{"), is(false));
        assertThat(checker.isBracketsBalanced("()["), is(false));
        assertThat(checker.isBracketsBalanced("{]"), is(false));
        assertThat(checker.isBracketsBalanced("}{"), is(false));
        assertThat(checker.isBracketsBalanced("(sadas(as{as)ass}asd)"), is(false));
        assertThat(checker.isBracketsBalanced("((qqq[{234]}888))"), is(false));
        assertThat(checker.isBracketsBalanced(""), is(true));
        assertThat(checker.isBracketsBalanced("asdasdasdasdasd asd sdf asr f"), is(true));
        assertThat(checker.isBracketsBalanced("[{s +sd a+s{sdas(afff)asd}dd}]"), is(true));
        assertThat(checker.isBracketsBalanced("{d1s5[asdasd]asa(22{323}33)(asdasd)}"), is(true));
    }
}
