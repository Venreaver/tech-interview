package com.epam.algo.datastructures;

import java.util.NoSuchElementException;

public class LinkedList<T> {
    private Node<T> first;
    private int size;

    public LinkedList() {
    }

    public int getSize() {
        return size;
    }

    public T getFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException("There are no elements in the list");
        }
        return first.getItem();
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void addFirst(T item) {
        first = new Node<>(item, first);
        ++size;
    }

    public boolean add(T item) {
        addFirst(item);
        return true;
    }

    public T removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException("There are no elements in the list");
        }
        T item = first.getItem();
        first = first.getNext();
        --size;
        return item;
    }

    public boolean contains(T item) {
        if (item != null) {
            for (Node<T> node = first; node != null; node = node.getNext()) {
                if (item.equals(node.getItem())) {
                    return true;
                }
            }
        } else {
            for (Node<T> node = first; node != null; node = node.getNext()) {
                if (node.getItem() == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public T get(int index) {
        if (!(index >= 0 && index < size)) {
            throw new IndexOutOfBoundsException(String.format("Index: %d, Size: %d", index, size));
        }
        Node<T> node = first;
        if (index > 0) {
            for (int i = 0; i < index; ++i) {
                node = node.getNext();
            }
        }
        return node.getItem();
    }

    public void clear() {
        for (int i = 0; i < size; ++i) {
            Node<T> node = first.getNext();
            first.setNext(null);
            first = node;
        }
        size = 0;
    }
}
