package com.epam.algo.datastructures;

public class DequeOnStack<T> {
    private Stack<T> in;
    private Stack<T> out;

    public DequeOnStack() {
        in = new Stack<>();
        out = new Stack<>();
    }

    public void add(T item) {
        addLast(item);
    }

    public T remove() {
        return removeFirst();
    }

    public T element() {
        return getFirst();
    }

    public void addFirst(T item) {
        out.push(item);
    }

    public void addLast(T item) {
        in.push(item);
    }

    public T removeFirst() {
        stackShift(in, out);
        return out.pop();
    }

    public T removeLast() {
        stackShift(out, in);
        return in.pop();
    }

    public T getFirst() {
        stackShift(in, out);
        return out.peek();
    }

    public T getLast() {
        stackShift(out, in);
        return in.peek();
    }

    public int getSize() {
        return in.getSize() + out.getSize();
    }

    public void clear() {
        in.clear();
        out.clear();
    }

    public boolean isEmpty() {
        return in.isEmpty() && out.isEmpty();
    }

    private void stackShift(Stack<T> from, Stack<T> to) {
        if (to.isEmpty()) {
            for (int i = 0, n = from.getSize(); i < n; ++i) {
                to.push(from.pop());
            }
        }
    }
}
