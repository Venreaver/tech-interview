package com.epam.algo.datastructures;

public class Stack<T> {
    private LinkedList<T> stackList;

    public Stack() {
        stackList = new LinkedList<>();
    }

    public int getSize() {
        return stackList.getSize();
    }

    public boolean isEmpty() {
        return stackList.isEmpty();
    }

    public void push(T item) {
        stackList.addFirst(item);
    }

    public T pop() {
        return stackList.removeFirst();
    }

    public T peek() {
        return stackList.getFirst();
    }

    public void clear() {
        stackList.clear();
    }
}
