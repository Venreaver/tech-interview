package com.epam.algo.datastructures;

import java.util.NoSuchElementException;

public class CircularQueue<T> {
    private T[] store;
    private int size;
    private int firstIndex;
    private int nextIndex;

    @SuppressWarnings("unchecked")
    public CircularQueue(int capacity) {
        store = (T[]) new Object[capacity];
    }

    public void add(T item) {
        if (isFull()) {
            throw new IllegalStateException("Queue is full!");
        }
        store[nextIndex] = item;
        nextIndex = (nextIndex + 1) % store.length;
        ++size;
    }

    public T remove() {
        T currentItem = element();
        store[firstIndex] = null;
        firstIndex = (firstIndex + 1) % store.length;
        --size;
        return currentItem;
    }

    public T element() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is empty!");
        }
        return store[firstIndex];
    }

    public void clear() {
        for (int i = 0; i < store.length; ++i) {
            store[i] = null;
        }
        size = 0;
        firstIndex = 0;
        nextIndex = 0;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private boolean isFull() {
        return store.length == size;
    }
}
