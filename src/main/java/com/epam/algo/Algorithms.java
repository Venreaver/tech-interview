package com.epam.algo;

import com.epam.algo.datastructures.Stack;
import javafx.util.Pair;

import java.util.Arrays;

public class Algorithms {
    public static void bubbleSort(int[] intArray) {
        bubbleSort(intArray, true);
    }

    public static void bubbleSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            for (int i = 0; i < intArray.length - 1; ++i) {
                boolean isSwapped = false;
                if (isAsc) {
                    for (int j = 0; j < intArray.length - 1 - i; ++j) {
                        if (intArray[j] > intArray[j + 1]) {
                            intSwapInArray(intArray, j, j + 1);
                            isSwapped = true;
                        }
                    }
                } else {
                    for (int j = 0; j < intArray.length - 1 - i; ++j) {
                        if (intArray[j] < intArray[j + 1]) {
                            intSwapInArray(intArray, j, j + 1);
                            isSwapped = true;
                        }
                    }
                }
                if (!isSwapped) {
                    break;
                }
            }
        }
    }

    public static void selectionSort(int[] intArray) {
        selectionSort(intArray, true);
    }

    public static void selectionSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            for (int i = 0; i < intArray.length - 1; ++i) {
                int minOrMax = i;
                if (isAsc) {
                    for (int j = minOrMax + 1; j < intArray.length; ++j) {
                        if (intArray[minOrMax] > intArray[j]) {
                            minOrMax = j;
                        }
                    }
                } else {
                    for (int j = minOrMax + 1; j < intArray.length; ++j) {
                        if (intArray[minOrMax] < intArray[j]) {
                            minOrMax = j;
                        }
                    }
                }
                intSwapInArray(intArray, minOrMax, i);
            }
        }
    }

    public static void insertionSort(int[] intArray) {
        insertionSort(intArray, true);
    }

    public static void insertionSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            for (int i = 1; i < intArray.length; ++i) {
                int key = intArray[i];
                int j = i;
                if (isAsc) {
                    while (j > 0 && intArray[j - 1] > key) {
                        intArray[j] = intArray[--j];
                    }
                } else {
                    while (j > 0 && intArray[j - 1] < key) {
                        intArray[j] = intArray[--j];
                    }
                }
                intArray[j] = key;
            }
        }
    }

    public static void quickSort(int[] intArray) {
        quickSort(intArray, true);
    }

    public static void quickSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            quickSort(intArray, 0, intArray.length - 1, isAsc);
        }
    }

    private static void quickSort(int[] intArray, int lowIndex, int highIndex, boolean isAsc) {
        if (highIndex > lowIndex) {
            int pivotIndex = partition(intArray, lowIndex, highIndex, isAsc);
            quickSort(intArray, lowIndex, pivotIndex, isAsc);
            quickSort(intArray, pivotIndex + 1, highIndex, isAsc);
        }
    }

    private static int partition(int[] intArray, int lowIndex, int highIndex, boolean isAsc) {
        int pivot = intArray[(highIndex + lowIndex) >>> 1];
        while (true) {
            if (isAsc) {
                while (intArray[lowIndex] < pivot) {
                    ++lowIndex;
                }
                while (intArray[highIndex] > pivot) {
                    --highIndex;
                }
            } else {
                while (intArray[lowIndex] > pivot) {
                    ++lowIndex;
                }
                while (intArray[highIndex] < pivot) {
                    --highIndex;
                }
            }
            if (highIndex <= lowIndex) {
                return highIndex;
            }
            intSwapInArray(intArray, lowIndex++, highIndex--);
        }
    }

    public static void mergeSort(int[] intArray) {
        if (intArray != null) {
            mergeSort(intArray, true);
        }
    }

    public static void mergeSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            mergeSort(intArray, 0, intArray.length, isAsc);
        }
    }

    private static void mergeSort(int[] intArray, int beginIndex, int endIndex, boolean isAsc) {
        if ((endIndex - beginIndex) < 2) {
            return;
        }
        int midIndex = (beginIndex + endIndex) >>> 1;
        mergeSort(intArray, beginIndex, midIndex, isAsc);
        mergeSort(intArray, midIndex, endIndex, isAsc);
        merge(intArray, beginIndex, midIndex, endIndex, isAsc);
    }

    private static void merge(int[] resultArray, int beginIndex,
                              int midIndex, int endIndex, boolean isAsc) {
        int firstIndex = 0;
        int secondIndex = midIndex - beginIndex;
        int resultIndex = beginIndex;
        int[] tempArray = new int[endIndex - beginIndex];
        System.arraycopy(resultArray, beginIndex, tempArray, 0, tempArray.length);

        if (isAsc) {
            while (firstIndex < (midIndex - beginIndex) && secondIndex < tempArray.length) {
                if (tempArray[secondIndex] < tempArray[firstIndex]) {
                    resultArray[resultIndex++] = tempArray[secondIndex++];
                } else {
                    resultArray[resultIndex++] = tempArray[firstIndex++];
                }
            }
        } else {
            while (firstIndex < (midIndex - beginIndex) && secondIndex < tempArray.length) {
                if (tempArray[secondIndex] > tempArray[firstIndex]) {
                    resultArray[resultIndex++] = tempArray[secondIndex++];
                } else {
                    resultArray[resultIndex++] = tempArray[firstIndex++];
                }
            }
        }
        while (firstIndex < (midIndex - beginIndex)) {
            resultArray[resultIndex++] = tempArray[firstIndex++];
        }
        while (secondIndex < tempArray.length) {
            resultArray[resultIndex++] = tempArray[secondIndex++];
        }
    }

    public static void radixSortLSD(int[] intArray) {
        radixSortLSD(intArray, true);
    }

    /**
     * RADIX SORT: Sort input array with radix sort algorithm
     * <p>
     * <p> Complexity: time -- O(nk) - O(nk) - O(nk), memory -- O(n + k)
     * <p> Supporting negative values sorting:
     * <p> 1. Divide input array on two: negative and positive
     * <p> 2. Mark all negative values in negative array as positive
     * <p> 3. Sort negative values as positive in reverse order
     * <p> 4. Mark values in negative array as negative
     * <p> 5. Merge of two arrays in the given order
     * <p> For arrays with only positive values just sort them without division in two arrays
     *
     * @param intArray input array which will be sorted
     * @param isAsc    is sorting in ascending order or not
     */
    public static void radixSortLSD(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            int min = Arrays.stream(intArray).min().getAsInt();
            if (min < 0) {
                int count = (int) Arrays.stream(intArray).filter(n -> n < 0).count();
                int[] negativeArray = new int[count];
                int[] positiveArray = new int[intArray.length - count];
                int positiveIndex = 0;
                int negativeIndex = 0;
                for (int number : intArray) {
                    if (number < 0) {
                        negativeArray[negativeIndex++] = -number;
                    } else {
                        positiveArray[positiveIndex++] = number;
                    }
                }
                positiveValuesRadixSortLSD(positiveArray, isAsc);
                positiveValuesRadixSortLSD(negativeArray, !isAsc);
                for (int i = 0; i < negativeArray.length; ++i) {
                    negativeArray[i] = -negativeArray[i];
                }
                if (isAsc) {
                    System.arraycopy(negativeArray, 0, intArray, 0, negativeArray.length);
                    System.arraycopy(positiveArray, 0, intArray, negativeArray.length, positiveArray.length);
                } else {
                    System.arraycopy(positiveArray, 0, intArray, 0, positiveArray.length);
                    System.arraycopy(negativeArray, 0, intArray, positiveArray.length, negativeArray.length);
                }
            } else {
                positiveValuesRadixSortLSD(intArray, isAsc);
            }
        }
    }

    /**
     * RADIX SORT: Sort input array with radix sort algorithm (for positive values)
     * <p>
     * <p> Complexity: time -- O(nk) - O(nk) - O(nk), memory -- O(n + k)
     * <p> 1. Get max value from array to define max digit position
     * <p> 2. Call counting sort for each digit position
     *
     * @param intArray input array which will be sorted
     * @param isAsc    is sorting in ascending order or not
     */
    private static void positiveValuesRadixSortLSD(int[] intArray, boolean isAsc) {
        int max = Arrays.stream(intArray).max().getAsInt();
        for (int digitPosition = 1; max / digitPosition > 0; digitPosition *= 10) {
            countSort(intArray, digitPosition, isAsc);
        }
    }

    /**
     * RADIX SORT: Counting sort for radix sort (in ascending and descending order)
     * <p>
     * <p> Complexity for counting sort: time -- O(n) - O(n) - O(n), memory -- O(n + k)
     *
     * @param intArray      input array which will be sorted
     * @param digitPosition digit position of number
     * @param isAsc         is sorting in ascending order or not
     */
    private static void countSort(int[] intArray, int digitPosition, boolean isAsc) {
        int result[] = new int[intArray.length];
        int[] count = new int[10];
        Arrays.fill(count, 0);
        for (int number : intArray) {
            ++count[(number / digitPosition) % 10];
        }
        if (isAsc) {
            for (int i = 1; i < count.length; ++i) {
                count[i] += count[i - 1];
            }
        } else {
            for (int i = 1; i < count.length; ++i) {
                count[count.length - i - 1] += count[count.length - i];
            }
        }
        for (int i = intArray.length - 1; i > -1; --i) {
            result[--count[(intArray[i] / digitPosition) % 10]] = intArray[i];
        }
        System.arraycopy(result, 0, intArray, 0, intArray.length);
    }

    public static int binarySearch(int[] intArray, int key) {
        if (intArray != null) {
            return binarySearch(intArray, 0, intArray.length - 1, key);
        }
        return -1;
    }

    private static int binarySearch(int[] intArray, int firstIndex, int lastIndex, int key) {
        while (firstIndex <= lastIndex) {
            int midIndex = (firstIndex + lastIndex) >>> 1;
            if (intArray[midIndex] < key) {
                firstIndex = midIndex + 1;
            } else if (intArray[midIndex] > key) {
                lastIndex = midIndex - 1;
            } else {
                return midIndex;
            }
        }
        return -1;
    }

    private static void intSwapInArray(int[] intArray, int firstIndex, int secondIndex) {
        int temp = intArray[firstIndex];
        intArray[firstIndex] = intArray[secondIndex];
        intArray[secondIndex] = temp;
    }

    public static void nonRecursiveMergeSort(int[] intArray) {
        if (intArray != null) {
            nonRecursiveMergeSort(intArray, true);
        }
    }

    public static void nonRecursiveMergeSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            nonRecursiveMergeSort(intArray, 0, intArray.length, isAsc);
        }
    }

    private static void nonRecursiveMergeSort(int[] intArray, int beginIndex, int endIndex, boolean isAsc) {
        for (int i = 1; i < intArray.length; i *= 2) {
            for (int j = 0; j < intArray.length - 1; j += 2 * i) {
                merge(intArray, j, j + i, Math.min(j + 2 * i, intArray.length), isAsc);
            }
        }
    }

    public static void nonRecursiveQuickSort(int[] intArray) {
        quickSort(intArray, true);
    }

    public static void nonRecursiveQuickSort(int[] intArray, boolean isAsc) {
        if (intArray != null) {
            nonRecursiveQuickSort(intArray, 0, intArray.length - 1, isAsc);
        }
    }

    private static void nonRecursiveQuickSort(int[] intArray, int lowIndex, int highIndex, boolean isAsc) {
        Stack<Pair<Integer, Integer>> stack = new Stack<>();
        stack.push(new Pair<>(lowIndex, highIndex));
        while (!stack.isEmpty()) {
            Pair<Integer, Integer> pair = stack.pop();
            int left = pair.getKey();
            int right = pair.getValue();
            if (right > left) {
                int pivot = partition(intArray, left, right, isAsc);
                if (pivot - left > right - pivot) {
                    stack.push(new Pair<>(left, pivot));
                    stack.push(new Pair<>(pivot + 1, right));
                } else {
                    stack.push(new Pair<>(pivot + 1, right));
                    stack.push(new Pair<>(left, pivot));
                }
            }
        }
    }
}
