package com.epam.algo;

import com.epam.algo.datastructures.Stack;

public class BracketsChecker {
    private static final String BRACKETS = "{}[]()";

    public boolean isBracketsBalanced(String bracketString) {
        Stack<Character> stackChecker = new Stack<>();
        char[] chars = bracketString.toCharArray();
        for (Character ch : chars) {
            int bracketIndex = BRACKETS.indexOf(ch);
            if (bracketIndex > -1) {
                if (bracketIndex % 2 == 0) {
                    stackChecker.push(ch);
                } else {
                    if (stackChecker.isEmpty()) {
                        return false;
                    }
                    int openIndex = BRACKETS.indexOf(stackChecker.pop());
                    if (bracketIndex != openIndex + 1) {
                        return false;
                    }
                }
            }
        }
        return stackChecker.isEmpty();
    }
}
