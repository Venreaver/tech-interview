package com.epam.algo.baskets;

import java.util.Objects;

public class Basket {
    private Color color;

    public Basket(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Basket basket = (Basket) o;
        return color == basket.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }
}
