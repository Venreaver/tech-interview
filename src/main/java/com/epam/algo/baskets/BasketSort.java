package com.epam.algo.baskets;

public class BasketSort {
    public static void sortBasketsLikeDutchFlag(Basket[] baskets) {
        if (baskets != null) {
            int redPointer = 0;
            int whitePointer = 0;
            int greenPointer = baskets.length - 1;
            while (whitePointer <= greenPointer) {
                switch (baskets[whitePointer].getColor()) {
                    case RED:
                        swap(baskets, redPointer++, whitePointer++);
                        break;
                    case WHITE:
                        ++whitePointer;
                        break;
                    case GREEN:
                        swap(baskets, whitePointer, greenPointer--);
                        break;
                }
            }
        }
    }

    public static void sortBaskets(Basket[] baskets) {
        if (baskets != null) {
            int redPointer = 0;
            int whitePointer = 0;
            int greenPointer = 0;
            int otherPointer = baskets.length - 1;
            while (greenPointer <= otherPointer) {
                switch (baskets[greenPointer].getColor()) {
                    case RED:
                        swap(baskets, redPointer++, greenPointer);
                        if (whitePointer < redPointer) {
                            ++whitePointer;
                        }
                        if (greenPointer < redPointer) {
                            ++greenPointer;
                        }
                        break;
                    case WHITE:
                        swap(baskets, whitePointer++, greenPointer++);
                        break;
                    case GREEN:
                        ++greenPointer;
                        break;
                    case OTHERCOLOR:
                        swap(baskets, greenPointer, otherPointer--);
                        break;
                }
            }
        }
    }

    private static void swap(Basket[] baskets, int firstIndex, int secondIndex) {
        Basket tempBasket = baskets[firstIndex];
        baskets[firstIndex] = baskets[secondIndex];
        baskets[secondIndex] = tempBasket;
    }
}
